# Radial Dirac wavefunctions in the Coulombic Hydrogen atom

This program plots the radial wavefunctions of the Dirac equation in a Coulombic nuclear potential. 
Expressions are taken from:
- Relativistic Quantum Mechanics: Book by Walter Greiner
https://link.springer.com/book/10.1007/978-3-662-04275-5
